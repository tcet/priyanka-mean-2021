const mongoose = require("mongoose");
const conn_str = "mongodb://localhost:27017/college"
// console.log("mongoose file running.....");

//connection to db
mongoose.connect(conn_str, { useNewUrlParser: true, useUnifiedTopology: true })
.then(() => console.log("Connected to db college......"))
.catch((e) => console.log(e));

// structure of your collection
const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    age: Number,
    city: String,
    class: String
});

//createing the collection
const user = new mongoose.model("user", userSchema);
// console.log(user);

 /* 1st way to insert documnet */
//  const u1 = new user({name:"tom",age:23,city:"mumbai"});
//  console.log(u1.save());

 /* 2nd way to insert document */
// const insertOneUser = async () =>{
//     try{
//         const u2 = new user({name:"max",age:21,city:"nagpur"});
//         const res = await u2.save();
//         console.log(res);

//     } catch(e) {
//         console.log(e);
//     }

// }
// insertOneUser();


/** Express Mongoose Integration **/

const express = require("express");
const { publicDecrypt } = require("crypto");
const app = express();

app.route("/user")
.get(async (req, res)=>{
    let data = await user.find(
        {$and: [{age : 21}, {city: {$ne: "mumbai"} }] }
    )
    .select({__v:0, _id:0})
    .limit()
    .sort({name:1});
    console.log(data);
    res.send(data);
})

.post(async (req,res)=>{
    r_data = req.query;
    let u3 = new user(req.query);
    let result = await u3.save();
    console.log(result);
    res.send(result);
})

.put(async (req, res)=>{
    r_data = req.query;
    let u_data = await user.updateMany({},{
        $set: {class: r_data.class}
    })
    res.send(u_data);
    console.log(u_data);
})

.delete(async (rq, rs)=>{
    d_cond = rq.query;
    let d_doc = await user.deleteMany({name: d_cond.name});
    rs.send(d_doc);
    console.log(d_doc);

})

app.listen(8989, ()=>{
    console.log("listening at 8989......");
});
