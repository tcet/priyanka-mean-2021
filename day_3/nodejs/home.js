const express = require("express");
const router = express.Router();

router.route("/")
.get((req,res)=>{
    res.setHeader('Content-Type','text/html');
    // res.send(" get request");
    data = req.query; // {num:2}
    sqr = data.num *  data.num; 
    console.log(data);
    res.send(`The square vaule is ${sqr}`);
})

.post((req,res)=>{
    res.setHeader('Content-Type', 'application/json');
    // res.send(" post request");
    data = req.body;
    res.send(data);

})

.put((req,res)=>{
    res.setHeader('Content-Type', 'text/html');
    res.send(" put request");
})

.delete((req,res)=>{
    res.setHeader('Content-Type', 'text/html');
    res.send(" delete request");
})

module.exports = router