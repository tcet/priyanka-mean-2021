var express = require("express");
var app = express();

console.log("express file running...");

app.get("/", (req,res)=>{
    res.setHeader('Content-Type', 'text/html');
    res.send("you are on home page");
});

app.get("/student", (req,res)=>{
    res.setHeader('Content-Type', 'text/html');
    res.send("you are on student page");
});

app.get("/teacher", (req,res)=>{
    res.setHeader('Content-Type', 'text/html');
    res.send("you are on teacher page");
});

app.post("/teacher", (req,res)=>{
    res.setHeader('Content-Type', 'text/html');
    res.send("teacher's post-add request");
});

app.put("/teacher", (req,res)=>{
    res.setHeader('Content-Type', 'text/html');
    res.send("teacher's put-update request");
});

app.delete("/teacher", (req,res)=>{
    res.setHeader('Content-Type', 'text/html');
    res.send("teacher's delete request");
});

const bodyParse = require('body-parser');
app.use(bodyParse.urlencoded({ extended: true}))
app.use(express.json());

app.route("/")
.get((req,res)=>{
    res.setHeader('Content-Type', 'text/html');
    res.send("home page get request");
})

.post((req,res)=>{
    res.setHeader('Content-Type', 'text/html');
    data = req.body;
    console.log(data);
    res.send(data);

})

.put((req,res)=>{
    res.setHeader('Content-Type', 'text/html');
    res.send("home page put request");
})

.delete((req,res)=>{
    res.setHeader('Content-Type', 'text/html');
    res.send("home page delete request");
})


app.listen(8989, ()=>{
    console.log("Listeining at port 8989.....");
});