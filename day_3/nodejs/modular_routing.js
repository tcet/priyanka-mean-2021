var express = require("express");
var app = express();
app.use(express.urlencoded({ extended: true}))
app.use(express.json());

var home = require("./home")

app.use("/", home);
// app.use("/student", student);
// app.use("/teacher", teacher);


app.post("/test", (req, res) => {
    res.send(req.body);
})

app.use((req,res)=>{
    res.status(404);
    res.send('the page is not found')

})

app.listen(8080, ()=>{
    console.log("Listeining at port 8080.....");
});
